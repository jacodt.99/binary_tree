#include "./include/type.h"
#include "./include/binary_search_tree_struct.h"
#include "./include/queue_struct.h"
#include "./include/binary_search_tree.h"
#include "./include/queue.h"

#include <iostream>

node* newNode(keytype key, datatype inf){
    node* n = new node;
    n->inf = inf;
    n->key = key;
    n->parent = NULL;
    n->rightChild = NULL;
    n->leftChild = NULL;
    return n;
}
void insert(BSTree& t, node* n){
    if (t == NULL){
        t = n;
        return;
    }
    if (compareKey(getKey(n), getKey(t)) < 0){
        if (getLeftChild(t) == NULL){
            n->parent = t;
            t->leftChild = n;
        }
        else
            insert(t->leftChild, n);    }
    else{
        if (getRightChild(t) == NULL){
            n->parent = t;
            t->rightChild = n;
        }
        else
            insert(t->rightChild, n);
    }
}

node* deleteNode(BSTree t, node* n){
    if (getLeftChild(n) == NULL)
        if (getRightChild(n) == NULL){
            delete n;
            n = NULL;
        }
        else{
            delete n;
            n = getRightChild(n);
        }
    else if(getRightChild(n) == NULL){
        delete n;
        n = getLeftChild(n);
    }
    else{
        node* tempNode = smallestSuccessor(n);
        n = tempNode;
        deleteNode(t, tempNode);
    }     
}

static node* smallestSuccessor(BSTree t){
    t = getRightChild(t);
    while(getLeftChild(t) != NULL)
        t = getLeftChild(t);
    return t;
}

keytype getKey(node* n){ return n->key; }
datatype getInf(node* n){ return n->inf; }
node* getParent(node* n){ return n->parent; }
node* getRightChild(BSTree t){ return t->rightChild; }
node* getLeftChild(BSTree t){ return t->leftChild; }


node* search(BSTree t, keytype key){
    if (compareKey(key, getKey(t)) < 0)
        search(getLeftChild(t), key);
    else if (compareKey(key, getKey(t)) > 0)
        search(getRightChild(t), key);   
    return t;
}

void inorderPrint(BSTree t){
    if (getLeftChild(t) != NULL)
        inorderPrint(getLeftChild(t));
        std::cout << getInf(t);
    if (getRightChild(t) != NULL)
        inorderPrint(getRightChild(t));
}

