#include "./include/type.h"
#include "./include/binary_search_tree_struct.h"
#include "./include/binary_search_tree.h"

#include <iostream>

using namespace std;

int main(void){

    BSTree root = newNode(0, "Luca");
    node* paolo = newNode(1, "Paolo");
    node* marco = newNode(2, "Marco");
    node* anna = newNode(3, "Anna");
    node* lucia = newNode(4,"Lucia");

    insert(root, paolo);
    insert(root, marco);
    insert(root, anna);
    insert(root, lucia);

    deleteNode(root, root);
    inorderPrint(root);
    return 0;
}