
cc = g++
binDir = ./bin/
CXXFLAGS = -Wall

$(binDir)output: $(binDir)main.o $(binDir)type.o $(binDir)binary_search_tree.o $(binDir)queue.o
	$(cc) $? -o $(binDir)output

$(binDir)main.o: main.cpp 
	$(cc) -c $< -o $@

$(binDir)type.o: type.cpp ./include/type.h 
	$(cc) -c $< -o $@

$(binDir)binary_search_tree.o: binary_search_tree.cpp ./include/binary_search_tree.h
	$(cc) -c $< -o $@

$(binDir)queue.o: queue.cpp ./include/queue.h
	$(cc) -c $< -o $@

clean:
	rm -rf $(binDir)*.o output
cleanall:
	rm -rf $(binDir)*