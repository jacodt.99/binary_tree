#ifndef BSTREE_H
#define BSTREE_H

struct node;
typedef node* BSTree;

//---------[ PRIMITIVE ]---------//

// [ creazione ]
node* newNode(keytype, datatype);
void insert(BSTree&, node*);
node* deleteNode(BSTree, node*);

// [ accesso ]
keytype getKey(node*);
datatype getInf(node*);
node* getParent(node*);
node* getRightChild(BSTree);
node* getLeftChild(BSTree);

//-----------[ VISITE ]-----------//

//-------[ ALTRE FUNZIONI ]-------//

node* search(BSTree, keytype);
void inorderPrint(BSTree);

//-----[ FUNZIONI STATICHE ]-----//
static node* smallestSuccessor(BSTree t);

#endif // BSTREE_H