#ifndef BSTREE_STRUCT_H
#define BSTREE_STRUCT_H

struct node {
    datatype inf;
    keytype key;
    node* parent;
    node* rightChild;
    node* leftChild;
};

typedef node* BSTree;

#endif // BSTREE_STRUCT_H