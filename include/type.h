#ifndef TYPE_H
#define TYPE_H

typedef const char* datatype;
typedef int keytype;

int compareData(datatype, datatype);
int compareKey(keytype, keytype);

#endif // TYPE_H