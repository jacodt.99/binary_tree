#ifndef QUEUE_H
#define QUEUE_H

struct elemBFS;
struct codaBFS;

codaBFS enqueue(codaBFS, node*);
node* dequeue(codaBFS&);
node* first(codaBFS);
bool isEmpty(codaBFS);
codaBFS newQueue();

static elemBFS* new_elem(node*);

#endif // QUEUE_H