#include "./include/type.h"
#include <cstring>

int compareData(datatype a, datatype b){
    return strcmp(a, b);
}

int compareKey(keytype a, keytype b){
    if (a > b) return 1;
    else if (a < b) return -1;
    else return 0;
}

void copyKey(keytype &a, keytype b){
    a = b;
}
